/*
** @description: Test class for FeesHierarchyBySelectionController class
*/
@isTest
private with sharing class FeesHierarchyChartControllerTest {
    
    private static List<String> departments = new List<String>{'Development', 'Marketing'};
    private static List<String> categories = new List<String>{'Coding', 'QA','ABM','Human Resource','Pre Sales'};
    private static List<String> subcategories = new List<String>{'Cat1', 'Cat2','Cat3'};
    private static List<String> types = new List<String>{'Type1', 'Type2','Type3','Type4'};

    /*
    ** @description: Setup Fees record to test with
    */
    @testSetup static void setup() {
        // Create Fees records
        List<Fees__c> testFees = new List<Fees__c>();
        for(Integer i=0;i<500;i++) {
            testFees.add(new Fees__c(
                    Department__c = departments[math.mod(i, departments.size())],
                    Category__c = categories[math.mod(i, categories.size())],
                    Sub_Category__c = subcategories[math.mod(i, subcategories.size())],
                    Type__c = types[math.mod(i, types.size())],
                    Unit_Price__c = (i+1)*3.5,
                    Quantity__c = i+2
                    ));
        }
        insert testFees;
    }

    @isTest static void testFeesHierarchyChartController() {

        Test.startTest();

        // The constructor will fetch all nodes (1 per records created)
        FeesHierarchyChartController feesChart = new FeesHierarchyChartController();

        // In the setup process, 500 records were inserted with varying combinations
        // So total possible combinations (multiple of all sizes of each array) should be returned:
        System.debug('feesChart.allNodeRecords.size: ' + feesChart.allNodeRecords.size());
        System.debug('feesChart.strAllNodeRecords: ' + feesChart.strAllNodeRecords);
        System.assertEquals(102,feesChart.allNodeRecords.size(), 'Incorrect number of nodes returned');
        System.assertNotEquals(NULL,feesChart.strAllNodeRecords, 'Serialized text was not returned as expected');

        Test.stopTest();
    }
}
