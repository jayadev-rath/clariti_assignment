/*
* @description: Controller class for the FeesHierarchy VF Page; Consumes only 1 query and Displays all combinations at once.
*/
public inherited sharing class FeesHierarchyBySelectionController {
    // For rendering all input fields
    public Fees__c fee {get;set;}
    public Decimal totalFees {get;set;}
    public String reportURL {get;set;}

    public FeesHierarchyBySelectionController() {
        fee = new Fees__c();
        totalFees = null;
        reportURL ='/';
    }

    public Pagereference updateFees() {
        totalFees = null;
        
        // Check all input parameters:
        String department = fee.Department__c != null ? fee.Department__c : '%';
        String category = fee.Category__c != null ? fee.Category__c : '%';
        String subCategory = fee.Sub_Category__c != null ? fee.Sub_Category__c : '%';
        String types = fee.Type__c != null ? fee.Type__c : '%';

        // Get the total fees for the selections:
        totalFees = Utilities.getTotalFromAggregate(FeesSelector.getAggregatedFeesStructure(department,category,subCategory,types));
        
        // Generate the report URL from the given filters:
        reportURL = createReportLink(department,category,subCategory,types);        
        
        return null;
    }

    /*
    ** @description: Prepare the report URL dynamically and apply multiple filters
    **      Example: /lightning/r/Report/00O5f000004vPAAEA2/view?fv0=Development&fv1=Coding&fv2=Cat1
    */
    @testVisible 
    private static String createReportLink(String department, String category, String subCategory, String types) {
        String reportUrl = '/lightning/r/Report/'+ Utilities.FEES_HIERARCHY_REPORT_ID +'/view?';

        if(department != '%')   reportUrl +=  '&fv0=' + EncodingUtil.urlEncode(department,'UTF-8');
        if(category != '%')     reportUrl +=  '&fv1=' + EncodingUtil.urlEncode(category,'UTF-8');
        if(subCategory != '%')  reportUrl +=  '&fv2=' + EncodingUtil.urlEncode(subCategory,'UTF-8');
        if(types != '%')        reportUrl +=  '&fv3=' + EncodingUtil.urlEncode(types,'UTF-8');
        
        return reportUrl;
    }
}