/*
* @description: Controller class for the FeesHierarchyChart VF Page; Consumes only 1 query and Displays all combinations at once in a org chart format
*/
public inherited sharing class FeesHierarchyChartController {

    @testVisible private List<NodeRecord> allNodeRecords {get;set;}
    public String strAllNodeRecords {
        get {
            return JSON.serialize(allNodeRecords);
        }
    }

    public FeesHierarchyChartController() {

        allNodeRecords = new List<NodeRecord>();
        Map<String,Decimal> feesByDepartment = new Map<String,Decimal>();
        Map<String,Decimal> feesByCategory = new Map<String,Decimal>();
        Map<String,Decimal> feesBySubCategory = new Map<String,Decimal>();
        Map<String,Decimal> feesByType = new Map<String,Decimal>();

        
        // Fetch the aggregated result of all fees:
        List<AggregateResult> allFeesList = FeesSelector.getAggregatedFeesStructure();

        // Iterate over all the fees and create a master map
        String dept,cat,subcat,types;
        Decimal fees;

        for(AggregateResult agFees: allFeesList) {
            dept = (String)agFees.get('Department__c');
            cat = (String)agFees.get('Category__c');
            subcat = (String)agFees.get('Sub_Category__c');
            types = (String)agFees.get('Type__c');
            fees = (Decimal)agFees.get('TotalFees');

            // Grouping by department:
            if(String.isNotEmpty(dept)) {
                feesByDepartment.put( dept,                             (feesByDepartment.get(dept) == null                         ? 0 : feesByDepartment.get(dept)) + fees);
            }
            if(String.isNotEmpty(cat)) {
                feesByCategory.put( dept+'_'+cat,                       (feesByCategory.get(dept+'_'+cat) == null                   ? 0 : feesByCategory.get(dept+'_'+cat)) + fees);
            }
            if(String.isNotEmpty(subcat)) {
                feesBySubCategory.put( dept+'_'+cat+'_'+subcat,         (feesBySubCategory.get(dept+'_'+cat+'_'+subcat) == null    ? 0 : feesBySubCategory.get(dept+'_'+cat+'_'+subcat)) + fees);
            }
            if(String.isNotBlank(types)) {
                feesByType.put( dept+'_'+cat+'_'+subcat+'_'+types,      (feesByType.get(dept+'_'+cat+'_'+subcat+'_'+types) == null   ? 0 : feesByType.get(dept+'_'+cat+'_'+subcat+'_'+types)) +fees);
            }
        }
        // Add all Department nodes:
        prepareChartData(feesByDepartment);
        // Add all Category nodes:
        prepareChartData(feesByCategory);
        // Add all SubCategory nodes:
        prepareChartData(feesBySubCategory);
        // Add all Type nodes:
        prepareChartData(feesByType);
    }

    /*
    ** @description: Method to prepare the Google Chart Node from the given map of fees
    */
    public void prepareChartData(Map<String,Decimal> feesMap) {
        
        //node id,name to be displayed, parent node id , additional info
        for(String grouping : feesMap.keySet()){
            String parentNodeId, nodeId, nodeName, nodeFees;
            Integer separtorIndex = grouping.lastIndexOf('_');
            // Only for parent groups (i.e. by departments) -1 will be returned as it's the first & only grouping
            // e.g: "Development" : 38833.49,
            if(separtorIndex == -1) {
                parentNodeId = 'clariti';
                nodeName = grouping;
            }
            // When sub-grouping (i.e. grouping by Dept/Category/SubCategory) exist, break the key to find all details
            // e.g. "Development_Quality Assurance_Cat3_TypeA" : 38833.49,
            else {                
                parentNodeId = grouping.substring(0, separtorIndex);
                nodeName = grouping.substring(separtorIndex+1, grouping.length());
            }
            nodeId = grouping;
            nodeFees = ''+feesMap.get(grouping);
            // Add the node detail to the record list:
            allNodeRecords.add(new NodeRecord(nodeId,parentNodeId,nodeName,nodeFees));
        }
    }

    /*
    ** @description: Class for handling the google chart node in the frontend
    */
    public class NodeRecord{
        public String nodeId {get;set;}
        public String parentNodeId {get;set;}
        public String nodeName {get;set;}
        public String nodeFees {get;set;}
        
        public NodeRecord(String nodeId, String parentNodeId, String nodeName, String nodeFees){
            this.nodeId = nodeId;
            this.nodeName = nodeName;
            this.parentNodeId = parentNodeId;
            this.nodeFees = nodeFees;
        }
    }
}