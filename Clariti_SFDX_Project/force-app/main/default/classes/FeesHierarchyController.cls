/*
* @description: Controller class for the FeesHierarchy VF Page; Consumes only 1 query and Displays all combinations at once.
*/
public inherited sharing class FeesHierarchyController {

    public Map<String,Decimal> feesByDepartment {get;set;}
    public Map<String,Decimal> feesByCategory {get;set;}
    public Map<String,Decimal> feesBySubCategory {get;set;}
    public Map<String,Decimal> feesByType  {get;set;}

    public FeesHierarchyController() {
        feesByDepartment = new Map<String,Decimal>();
        feesByCategory = new Map<String,Decimal>();
        feesBySubCategory = new Map<String,Decimal>();
        feesByType = new Map<String,Decimal>();

        
        // Fetch the aggregated result of all fees:
        List<AggregateResult> allFeesList = FeesSelector.getAggregatedFeesStructure();

        // Iterate over all the fees and create a master map
        String dept,cat,subcat,types;
        Decimal fees;


        for(AggregateResult agFees: allFeesList) {
            dept = (String)agFees.get('Department__c');
            cat = (String)agFees.get('Category__c');
            subcat = (String)agFees.get('Sub_Category__c');
            types = (String)agFees.get('Type__c');
            fees = (Decimal)agFees.get('TotalFees');

            // Grouping by department:
            if(String.isNotEmpty(dept)) {
                feesByDepartment.put( dept,                             (feesByDepartment.get(dept) == null                         ? 0 : feesByDepartment.get(dept)) + fees);
            }
            if(String.isNotEmpty(cat)) {
                feesByCategory.put( dept+'_'+cat,                       (feesByCategory.get(dept+'_'+cat) == null                   ? 0 : feesByCategory.get(dept+'_'+cat)) + fees);
            }
            if(String.isNotEmpty(subcat)) {
                feesBySubCategory.put( dept+'_'+cat+'_'+subcat,         (feesBySubCategory.get(dept+'_'+cat+'_'+subcat) == null    ? 0 : feesBySubCategory.get(dept+'_'+cat+'_'+subcat)) + fees);
            }
            if(String.isNotBlank(types)) {
                feesByType.put( dept+'_'+cat+'_'+subcat+'_'+types,      (feesByType.get(dept+'_'+cat+'_'+subcat+'_'+types) == null   ? 0 : feesByType.get(dept+'_'+cat+'_'+subcat+'_'+types)) +fees);
            }

        }
    }
}