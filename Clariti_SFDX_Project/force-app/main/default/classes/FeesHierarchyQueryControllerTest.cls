/*
** @description: Test class for FeesHierarchyBySelectionController class
*/
@isTest
private with sharing class FeesHierarchyQueryControllerTest {
    
    private static List<String> departments = new List<String>{'Development', 'Marketing'};
    private static List<String> categories = new List<String>{'Coding', 'Quality Assurance','ABM','Human Resource','Pre Sales'};
    private static List<String> subcategories = new List<String>{'Cat1', 'Cat2','Cat3'};
    private static List<String> types = new List<String>{'TypeA', 'TypeB','TypeC','TypeD'};

    /*
    ** @description: Setup Fees record to test with
    */
    @testSetup static void setup() {
        // Create Fees records
        List<Fees__c> testFees = new List<Fees__c>();
        for(Integer i=0;i<200;i++) {
            testFees.add(new Fees__c(
                    Department__c = departments[math.mod(i, departments.size())],
                    Category__c = categories[math.mod(i, categories.size())],
                    Sub_Category__c = subcategories[math.mod(i, subcategories.size())],
                    Type__c = types[math.mod(i, types.size())],
                    Unit_Price__c = (i+1)*3.5,
                    Quantity__c = i+2
                    ));
        }
        System.debug('$$$$' + testFees[0] + testFees[1] + testFees[2]);
        insert testFees;
    }

    @isTest static void testFeesHierarchyQueryController_PositiveUsecase() {

        Test.startTest();

        // The constructor will fetch all nodes (1 per records created)
        FeesHierarchyQueryController feesHierarchyQuery = new FeesHierarchyQueryController();

        // Check for Development department, QA category & Cat1 Sub Category, TypeA type
        feesHierarchyQuery.inputQueryFromPage = 'What are the total for TypeA type Cat1 fees within Coding Category of the Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertEquals('Development', feesHierarchyQuery.dept,'Department Mismatched');
        System.assertEquals('Coding', feesHierarchyQuery.cat,'Category Mismatched');
        System.assertEquals('Cat1', feesHierarchyQuery.subcat,'SubCategory Mismatched');
        System.assertEquals('TypeA', feesHierarchyQuery.types,'Type Mismatched');
        System.assertNOTEquals(0,feesHierarchyQuery.totalFees,'Total Fees should not be zero');

        Test.stopTest();
    }

    @isTest static void testFeesHierarchyQueryController_MissingDepartment() {

        Test.startTest();

        // The constructor will fetch all nodes (1 per records created)
        FeesHierarchyQueryController feesHierarchyQuery = new FeesHierarchyQueryController();

        // Check for incorrect department Develop instead of Development:
        feesHierarchyQuery.inputQueryFromPage = 'What are the total for Type1 type Cat1 fees within QA Category of the Develop department?';
        
        feesHierarchyQuery.fetchTotalFees();

        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for missing department');
        System.assertEquals(-1,feesHierarchyQuery.totalFees,'Total Fees must be -1 for errors');

        Test.stopTest();
    }

    @isTest static void testFeesHierarchyQueryController_MissingQueryAndMultipleMatches() {

        Test.startTest();

        // The constructor will fetch all nodes (1 per records created)
        FeesHierarchyQueryController feesHierarchyQuery = new FeesHierarchyQueryController();

        // Check for incorrect department Develop instead of Development:
        //feesHierarchyQuery.inputQueryFromPage = 'What are the total for Type1 type Cat1 fees within QA Category of the Develop department?';
        
        feesHierarchyQuery.fetchTotalFees();

        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for missing department');
        System.assertEquals(-1,feesHierarchyQuery.totalFees,'Total Fees must be -1 for errors');


        // Test multiple departments
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the Development & Marketing department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple department');

        // Test multiple Categories
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the Quality Assurance & ABM category in Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple category');
        
        // Test multiple Sub Categories
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the Cat1 & Cat2 & Cat3 sub category in Quality Assurance category in Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple subcategory');
        
        // Test multiple Types
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the TypeA ,TypeB,TypeD,TypeC type in Cat2 sub category in Quality Assurance category in Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple types');
                
        // Test where category is missing but has Type/SubCategory
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the TypeB type in Cat2 sub category in Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple types');

        // Test where sub category is missing but has Type
        feesHierarchyQuery.inputQueryFromPage = 'What are the total fees for the TypeB type in Quality Assurance category in Development department?';
        feesHierarchyQuery.fetchTotalFees();
        System.assertNOTEquals(NULL, feesHierarchyQuery.errorMessage,'ErrorMessage was not returned for multiple types');

        Test.stopTest();
    }
}
