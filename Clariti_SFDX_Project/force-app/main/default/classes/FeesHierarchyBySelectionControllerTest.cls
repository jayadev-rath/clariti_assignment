/*
** @description: Test class for FeesHierarchyBySelectionController class
*/
@isTest
private with sharing class FeesHierarchyBySelectionControllerTest {
    
    private static List<String> departments = new List<String>{'Development', 'Marketing'};
    private static List<String> categories = new List<String>{'Coding', 'QA','ABM','Human Resource','Pre Sales'};
    private static List<String> subcategories = new List<String>{'Cat1', 'Cat2','Cat3'};
    private static List<String> types = new List<String>{'Type1', 'Type2','Type3','Type4'};

    /*
    ** @description: Setup Fees record to test with
    */
    @testSetup static void setup() {
        // Create Fees records
        List<Fees__c> testFees = new List<Fees__c>();
        for(Integer i=0;i<5;i++) {
            testFees.add(new Fees__c(
                    Department__c = departments[math.mod(i, departments.size())],
                    Category__c = categories[math.mod(i, categories.size())],
                    Sub_Category__c = subcategories[math.mod(i, subcategories.size())],
                    Type__c = types[math.mod(i, types.size())],
                    Unit_Price__c = (i+1)*3.5,
                    Quantity__c = i+2
                    ));
        }
        insert testFees;
    }

    @isTest static void testFeesHierarchyBySelectionController() {

        Test.startTest();

        FeesHierarchyBySelectionController feesBySelection = new FeesHierarchyBySelectionController();
        feesBySelection.fee.Department__c = departments[1];
        feesBySelection.fee.Category__c = categories[0];
        feesBySelection.fee.Sub_Category__c = subcategories[2];
        feesBySelection.fee.Type__c = types[0];

        feesBySelection.updateFees();
        // This combination doesn't exist, so it must return 0;
        System.assertEquals(0, feesBySelection.totalFees, 'Expected 0 as total fees. Got non-zero number.');

        // update the selections to select all first values:
        feesBySelection.fee.Department__c = departments[0];
        feesBySelection.fee.Category__c = categories[0];
        feesBySelection.fee.Sub_Category__c = subcategories[0];
        feesBySelection.fee.Type__c = types[0];
        feesBySelection.updateFees();

        // This must return a non-zero value
        System.assertNotEquals(0, feesBySelection.totalFees,'Non-zero value expected, got zero.');

        Test.stopTest();
    }
}
