/*
** @description: Store all utility methods in a central class for ease of use & distribution
*/
public without sharing class Utilities {

    public static String FEES_HIERARCHY_REPORT_ID ='00O5f000004vPAAEA2';
    
    /*
    ** @The selector layer will return only 1 aggregate result record. Find and return the total fee from that record 
    */
    public static Decimal getTotalFromAggregate(List<AggregateResult> agResult){
        for(AggregateResult agr :agResult){
            if(agr.get('TotalFees')!= null)
                return (Decimal)agr.get('TotalFees');
        }
        // return 0 as Fail-Safe:
        return 0;
    }

    /*
    ** @Description: return all matched strings from an array by comparing with the input query
    */
    public static List<String> findMatches(List<String> inpString, String inputQuery){
        //String inputQuery = 'What are the total Cat1 fees within Quality Assurance Category of the Development department?';
        List<String> matchedStrings = new List<String>();
        Pattern p = Pattern.compile('(?i)('+ String.join(inpString,'|') +')');
        Matcher m = p.matcher(inputQuery);
        while (m.find()) {
            matchedStrings.add(m.group(0));
        }
        return matchedStrings;
    }
}