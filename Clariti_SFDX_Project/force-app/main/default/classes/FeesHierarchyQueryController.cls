/*
* @description: Controller class for the FeesHierarchyByQuery VF Page
*       Supports chatting type of interface for the user to get the total fees for a combination of Dept/Category/subcategory/type...etc
*/
public inherited sharing class FeesHierarchyQueryController {
    // For capturing user input 
    public String inputQueryFromPage {get;set;}
    // For displaying any error messages
    public String errorMessage{get;set;}
    // For displaying total fees back in the vf page
    public Decimal totalFees {get;set;}
    public String dept {get;set;}
    public String cat {get;set;}
    public String subcat {get;set;}
    public String types {get;set;}
        
    // Page Constructor
    public FeesHierarchyQueryController() {
        totalFees = -1;
    }

    // Action method to get all fees
    public PageReference fetchTotalFees(){
        errorMessage = null;
        totalFees = -1;
        
        // Check if the user has specified a query or not:
        if(String.isEmpty(inputQueryFromPage)) {
            errorMessage = 'Query can not be blank.';
        } else {
            // Get the total fees
            totalFees = returnTotalFees(inputQueryFromPage);
        }
        
        // Check if any error was encountered:
        if(errorMessage != null) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR,errorMessage));
        }
        return null;
    }

    /*
    ** @description: Parse the input query against all Department/Categories/SubCategories & Types. Then return the aggregated result with that filter
    ** Example Query:  What are the total Cat1 fees within Quality Assurance Category of the Development department?'; 
    ** Result: 112021 ( for Branch Development, Category QA, SubCategory Cat1 )
    */
    public Decimal returnTotalFees(String inputQuery){
        //Get all Categories, Subcategories and Type values if only Department is specified
        cat='%';
        subCat='%';
        types='%';

        // Find all departments specified in the input query:
        List<String> matchedDepartments = Utilities.findMatches(FeesSelector.getDepartments(), inputQuery);
        
        // Check if multiple departments are specified:
        if(matchedDepartments.size() == 0) {
            errorMessage = 'Please specify a Department in your query.';
        }
        // One department has been specified, proceed to next step:
        else if(matchedDepartments.size() == 1) {
            dept = matchedDepartments[0];
            List<String> matchedCategories = Utilities.findMatches(FeesSelector.getCategories(), inputQuery);
            List<String> matchedSubCategories = Utilities.findMatches(FeesSelector.getSubCategories(), inputQuery);
            List<String> matchedTypes = Utilities.findMatches(FeesSelector.getTypes(), inputQuery);
    
            // If no categories are mentioned, but subcategories/types are mentioned, throw an error
            if(matchedCategories.size() == 0 && (matchedSubCategories.size() > 0 || matchedTypes.size() > 0 )) {
                errorMessage = 'You have specified a Sub Category/Type ('+String.join(matchedSubCategories,',')+ String.join(matchedTypes,',')+'), but missing a Category. Please specify a Category in order to specify a Sub Category/Type';
            }
            // One Category has been specified, proceed to next step:
            else if (matchedCategories.size() == 1) {
                cat = matchedCategories[0];

                // If no sub categories are mentioned, but types are mentioned, throw an error
                if(matchedSubCategories.size() == 0 && matchedTypes.size() > 0 ) {
                    errorMessage = 'You have specified a Type ('+ String.join(matchedTypes,',')+'), but missing a Sub Category. Please specify a Sub Category in order to specify a Type';
                }
                
                // One Sub Category has been specified, proceed to next step:
                else if(matchedSubCategories.size() == 1) {
                    subCat = matchedSubCategories[0];
                    // If only one Type mentioned, go ahead with the query
                    if(matchedTypes.size() == 1){
                        types = matchedTypes[0];
                    }
                    // If more than 1 Type value found, throw an error
                    // don't use 'else' here. It has to be >1 for generating an error. In case nothing is specified, use '%' instead.
                    else if(matchedTypes.size() > 1){
                        errorMessage = 'Multiple Types detected ('+String.join(matchedTypes,',')+'). Please specify only one Type value that you want to check for.';
                    }
                }
                // Throw error for >1 Sub Categories
                // don't use 'else' here. It has to be >1 for generating an error. In case nothing is specified, use '%' instead.
                else if(matchedSubCategories.size() > 1) {
                    errorMessage = 'Multiple Sub Categories detected ('+String.join(matchedSubCategories,',')+'). Please specify only one Sub Category that you want to check for.';
                }
            }
            // Throw error for >1 Categories
            else if(matchedCategories.size() > 1) {
                errorMessage = 'Multiple Categories detected ('+String.join(matchedCategories,',')+'). Please specify only one Category that you want to check for.';
            }
        }
        // Throw error for >1 Departments
        else {
            errorMessage = 'Multiple Departments detected ('+String.join(matchedDepartments,',')+'). Please specify only one department that you want to check for.';
        }

        System.debug('Error found:' + errorMessage);
        //if no errors found, then find the result:
        if(errorMessage == null){
            totalFees = Utilities.getTotalFromAggregate(FeesSelector.getAggregatedFeesStructure(dept,cat,subCat,types));
        }


        // for multiple departments : Which department did you mean? Please specify one department at a time
        return totalFees;
    }
    
    /*
    ** @The selector layer will return only 1 aggregate result record. Find and return the total fee from that record 
    */
    /*private static Decimal getTotalFromAggregate(List<AggregateResult> agResult){
        for(AggregateResult agr :agResult){
            if(agr.get('TotalFees')!= null)
                return (Decimal)agr.get('TotalFees');
        }
        // return 0 as Fail-Safe:
        return 0;
    }*/

    /*
    ** @Description: return all matched strings from an array by comparing with the input query
    */
    /*private static List<String> findMatches (List<String> inpString, String inputQuery){
        //String inputQuery = 'What are the total Cat1 fees within Quality Assurance Category of the Development department?';
        List<String> matchedStrings = new List<String>();
        Pattern p = Pattern.compile('(?i)('+ String.join(inpString,'|') +')');
        Matcher m = p.matcher(inputQuery);
        while (m.find()) {
            matchedStrings.add(m.group(0));
        }
        return matchedStrings;
    }*/
}