/*
* Selector class for Fees__c object.
*/
public without sharing class FeesSelector {
    
    /*
    * @description: Get the total fees (surcharge applied) by Department and all categories
    */
    public static List<AggregateResult> getAggregatedFeesStructure() {
        return [SELECT Department__c, Category__c, Sub_Category__c, Type__c, SUM(Base_Total_with_Surcharge__c) TotalFees
                FROM Fees__c GROUP BY Department__c, Category__c, Sub_Category__c, Type__c 
                ORDER BY Department__c ASC, Category__c ASC, Sub_Category__c ASC, Type__c ASC
        ];
    }

    /*
    * @description: Get the total fees (surcharge applied) by specified Department/Category...etc
    */
    public static List<AggregateResult> getAggregatedFeesStructure(String department, String category, String subCategory, String Type) {
        return [SELECT SUM(Base_Total_with_Surcharge__c) TotalFees 
                    FROM Fees__c 
                    WHERE Department__c LIKE :department AND
                    Category__c LIKE :category AND
                    Sub_Category__c LIKE :subCategory AND
                    Type__c LIKE :type
        ];
    }

    /*
    ** @description: Return all available Departments
    */
    public static List<String> getDepartments(){
        return getAllPicklistValues(Fees__c.Department__c.getDescribe());
    }
    
    /*
    ** @description: Return all available Categories
    */
    public static List<String> getCategories(){
        return getAllPicklistValues(Fees__c.Category__c.getDescribe());
    }
    
    /*
    ** @description: Return all available Sub Categories
    */
    public static List<String> getSubCategories(){
        return getAllPicklistValues(Fees__c.Sub_Category__c.getDescribe());
    }

    /*
    ** @description: Return all available Types
    */
    public static List<String> getTypes(){
        return getAllPicklistValues(Fees__c.Type__c.getDescribe());
    }
    
    /*
    ** @description: Return all available active values for a picklist
    */

    private static List<String> getAllPicklistValues(Schema.DescribeFieldResult fieldResult){
        List<String> pickListValues= new List<String>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if (pickListVal.isActive()) {
                // Using label for user friendly interaction:
                pickListValues.add(pickListVal.getLabel());
            }
        }
        return pickListValues;
    }
}