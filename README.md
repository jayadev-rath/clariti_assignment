# Clariti Technical Interview

## Task 1: Coding
Using design and development tools, languages, and techniques you are most familiar with create an objected orientated console application solution that given a tabular set of fees constructs a tree
representing the hierarchy nature of the fees and then performs specific calculations to determine the total fees amount for a given node within the tree.
Your application should be able to answer questions similar to those shown below:

### What are the total Cat1 fees within Quality Assurance Category of the Development department? 
Ans:110212


### What are the total fees for the Human Resources category of the Operations department? 
Ans: 229041

A fee’s base total is calculated as:
## Fee Total = Unit Price x Quantity


Additionally, fees within each department are subject to additional surcharges as per the table below.
## Department Surcharge
* Marketing Total + 10%
* Sales Total +15%
* Development Total +20%
* Operations Total – 15%
* Support Total – 5%

Please do not use third party libraries to implement algorithms or data structures. However, please
feel free to use any library you need to load the CSV data into your application

## Fee Hierarchy
Fees shall be modelled using the example hierarchy below. (Please note this is a partial view)

# Process
1. Take home design and coding assignment (3 days to complete)
2. Assignment review and collaborate design session 45 mins conference call

# Deliverables
• Source code
• Unit tests

# What we are looking for
• Understanding or Data Structures and Algorithms
• Design considers performance and scalability and operation in enterprise scenarios.
• Design follows industry best practices and design patterns
• Design supports extensibility
• Coding implementation has 75% unit test coverage.

# We will be assessing
• Completeness of task
• Code quality
• Algorithm efficiency
• Unit Test Quality and Coverage

